<?php

/**
 * Deployer Script
 * 
 * @author Gema Aji Wardian
 */

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'app-education-connection');

// Project repository
set('repository', 'git@gitlab.com:sobat-system/education-connection-backend.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', ['public/upload']);

// Writable dirs by web server 
add('writable_dirs', ['public/upload']);

// Hosts
host('development')
    ->hostname('165.22.55.167')
    ->stage('development')
    ->port(11511)
    ->user('ubuntu')
    ->set('branch', 'master')
    ->set('deploy_path', '/var/www/app-education-connection.sobatteknologi.com/htdocs/');

// Tasks

task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    //    'deploy:vendors-dev',
    //    'deploy:phpunit',
    //    'deploy:security-checker',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:symlink',
    'cleanup',
])->desc('Deploy your project');

task('reload:php-fpm', function () {
    run('sudo /usr/sbin/service php7.4-fpm reload');
});

after('deploy', 'reload:php-fpm');
