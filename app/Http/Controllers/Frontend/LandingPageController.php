<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Content;
use App\Models\Menu;
use App\Models\Preference;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function index() {
        $banner = Banner::all();
        $content = Content::all();
        $menu = Menu::all();
        $preference = Preference::all();

        $array = [
            $banner,
            $content,
            $menu,
            $preference
        ];

        return response()->json([
            'status' => true | false,
            'message' => 'Data berhasil di tampilkan',
            'data'    => $array
        ]);
        
    }
}
