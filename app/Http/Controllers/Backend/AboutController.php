<?php

namespace App\Http\Controllers\Backend;

use App\Models\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\UploadRepository;
use App\Models\Preference;

class AboutController extends Controller
{
    protected $upload;
    public function __construct(UploadRepository $upload) {
        $this->upload = $upload;
    }

    public function index()
    {
        $data = Content::where('type', 'about')->first();

        return view('backend.pages.about.index', compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'image'          => 'required|mimes:jpg,png,jpeg,svg|max:1000',
            'title'          => 'required',
            'description'    => 'required'
        ]);

        if($request->file('image')){
            $image  = $this->upload->save($request->file('image'));
        }else{
            $image  = null;
        }

        Content::create([
            'type'      => 'about',
            'attribute' => array_merge($request->except(['_token']), ['image' => $image])
        ]);

        return redirect()->route('admin.about.index')->with(['create' => 'create']);
    }

    public function update(Request $request, Content $content)
    {
        $request->validate([
            'image'          => 'mimes:jpg,png,jpeg,svg|max:1000',
            'title'          => 'required',
            'description'    => 'required'
        ]);

        if($request->file('image')){
            $image = $this->upload->update($content->image, $request->file('image'));
        }else{
            $image = $content->image;
        }

        $content->update([
            'type'      => 'about',
            'attribute' => array_merge($request->except(['_token']), ['image' => $image])
        ]);

        return redirect()->route('admin.about.index')->with(['update' => 'update']);
    }
}
