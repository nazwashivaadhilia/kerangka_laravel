<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repository\UploadRepository;
use Illuminate\Http\Request;
use App\Models\Menu;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    // controller CRUD menu
    public function index()
    {
        $menu = DB::table('menu')->get();
        return view('backend.pages.menus.index', ['menu' => $menu]);
    }
}
