<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\Repository\UploadRepository;
use App\Models\Menu;

class VisiController extends Controller
{
    protected $upload;
    public function __construct(UploadRepository $upload)
    {
        $this->upload = $upload;
    }

    public function index()
    {
        if (request()->ajax()) {
            $data = Menu::latest()->get();
            return DataTables::of($data)->make(true);
        }

        return view('backend.pages.menu.index');
    }

    public function create()
    {
        $data = null;
        return view('backend.pages.menu.action', compact('data'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'nomor_menu'            => 'required',
            'nama_menu'             => 'required',
            'deskripsi'             => 'required',
            'harga'                 => 'required',
        ]);

        Menu::create([
            'nomor_menu'            => $request->nomor_menu,
            'nama_menu'            => $request->nama_menu,
            'deskripsi'            => $request->deskripsi,
            'harga'            => $request->harga,
        ]);


        return redirect()->route('admin.menu.index')->with(['created' => 'created']);
    }

    public function edit(Menu $menu)
    {
        $data = $menu;

        return view('backend.pages.menu.action', compact('data'));
    }

    public function update(Request $request, Menu $menu)
    {
        request()->validate([
            'nomor_menu'            => 'required',
            'nama_menu'             => 'required',
            'deskripsi'             => 'required',
            'harga'                 => 'required',
        ]);

        $menu->update([
            'nomor_menu'            => $request->nomor_menu,
            'nama_menu'            => $request->nama_menu,
            'deskripsi'            => $request->deskripsi,
            'harga'            => $request->harga,
        ]);

        return redirect()->route('admin.menu.index')->with(['updated' => 'updated']);
    }

    public function delete(Menu $menu)
    {
        $this->upload->delete($menu->all());

        $menu->delete();
    }
}
