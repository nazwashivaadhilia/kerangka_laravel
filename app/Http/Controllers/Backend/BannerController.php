<?php

namespace App\Http\Controllers\Backend;

use App\Models\Banner;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\Repository\UploadRepository;

class BannerController extends Controller
{
    protected $upload;
    public function __construct(UploadRepository $upload) {
        $this->upload = $upload;
    }
    
    public function index()
    {
        if(request()->ajax()){
            $data = Banner::latest()->get();
            return DataTables::of($data)->make(true);
        }

        return view('backend.pages.banner.index');
    }

    public function create()
    {
        $data = null;
        return view('backend.pages.banner.action', compact('data'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'image'             => 'mimes:jpg,png,jpeg|max:3000',
        ]);

        if($request->file('image')){
            $image  = $this->upload->save($request->file('image'));
        }else{
            $image  = null;
        }
        
        Banner::create([
            'image'            => $image,
        ]);

        return redirect()->route('admin.banner.index')->with(['created' => 'created']);
    }

    public function edit(Banner $banner)
    {
        $data = $banner;

        return view('backend.pages.banner.action', compact('data'));
    }

    public function update(Request $request , Banner $banner)
    {
        request()->validate([
            'image'             => 'mimes:jpg,png,jpeg|max:3000',
        ]);

        if($request->file('image')){
            $image = $this->upload->update($banner->image,$request->file('image'));
        }else{
            $image = $banner->image;
        }
        
        $banner->update([
            'image'            => $image,
        ]);

        return redirect()->route('admin.banner.index')->with(['updated' => 'updated']);
    }

    public function delete(Banner $banner)
    {
        $this->upload->delete($banner->image);

        $banner->delete();
    }
}
