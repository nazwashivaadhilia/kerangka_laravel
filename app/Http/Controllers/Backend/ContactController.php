<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\UploadRepository;
use App\Models\Preference;

class ContactController extends Controller
{
    protected $upload;
    public function __construct(UploadRepository $upload)
    {
        $this->upload = $upload;
    }

    public function index()
    {
        $data = Preference::where('type', 'contact')->first();
        return view('backend.pages.contact.index', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $preference = Preference::where('id', $id)->first();
        $request->validate([
            'image'          => 'mimes:jpg,png,jpeg,svg|max:1000',
            'title'          => 'required',
            'description'    => 'required',
            'phone'            => 'required',
            'email'            => 'required',
            'loc'            => 'required'
        ]);

        if ($request->file('image')) {
            $image = $this->upload->update($preference->image, $request->file('image'));
        } else {
            $image = $preference->image;
        }

        $preference->update([
            'type'      => 'contact',
            'Atribute' => [
                'image' => $image,
                'title' => $request->title,
                'description' => $request->description,
                'email' => $request->email,
                'phone' => $request->phone,
                'loc' => $request->loc,
            ]
        ]);


        return redirect()->route('admin.contact.index')->with(['updated' => 'updated']);
    }
}
