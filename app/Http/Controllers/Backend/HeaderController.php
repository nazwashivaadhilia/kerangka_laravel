<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\UploadRepository;
use App\Models\Preference;

class HeaderController extends Controller
{
    protected $upload;
    public function __construct(UploadRepository $upload)
    {
        $this->upload = $upload;
    }

    public function index()
    {
        $data = Preference::where('type', 'header')->first();
        return view('backend.pages.header.index', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $preference = Preference::where('id', $id)->first();
        $request->validate([
            'image'          => 'mimes:jpg,png,jpeg,svg|max:1000',
            'title'          => 'required',
            'description'    => 'required',
        ]);

        if ($request->file('image')) {
            $image = $this->upload->update($preference->image, $request->file('image'));
        } else {
            $image = $preference->image;
        }

        $preference->update([
            'type'      => 'header',
            'Atribute' => [
                'image' => $image, // Use the updated image variable here
                'title' => $request->title,
                'description' => $request->description,
            ]
        ]);

        return redirect()->route('admin.header.index')->with(['updated' => 'updated']);
    }
}
























