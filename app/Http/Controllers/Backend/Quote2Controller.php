<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\UploadRepository;
use App\Models\Preference;

class Quote2Controller extends Controller
{
    protected $upload;
    public function __construct(UploadRepository $upload)
    {
        $this->upload = $upload;
    }

    public function index()
    {
        $data = Preference::where('type', 'quote2')->first();
        return view('backend.pages.quote2.index', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $preference = Preference::where('id', $id)->first();
        $request->validate([
            'title'          => 'required',
        ]);

        $preference->update([
            'type'      => 'quote2',
            'Atribute' => [
                'title' => $request->title,
            ]
        ]);

        return redirect()->route('admin.quote2.index')->with(['updated' => 'updated']);
    }
}







