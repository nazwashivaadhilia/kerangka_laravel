<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    use HasFactory;
    protected $table = 'preference';
    protected $guarded = [];
    protected $casts = [
        'Atribute' => 'array'
    ];
}
