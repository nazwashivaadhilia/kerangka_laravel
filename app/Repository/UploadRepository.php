<?php
namespace App\Repository;

use Illuminate\Support\Facades\File;
interface UploadInterface
{
    public function save($image);

    public function update($old_image,$image);

    public function delete($image);

    public function saveFlag($image);

    public function updateFlag($old_image,$image);
}

class UploadRepository implements UploadInterface
{
    public function save($image)
    {
        $file       = $image;
        $file_name  = url('upload/img/'.$file->hashName());
        $destinasi  = 'upload/img/';
        $file->move($destinasi, $file_name);

        return $file_name;
    }

    public function update($old_image, $image)
    {
        $array = explode('/',$old_image);

        if(isset($array[5])){
            File::delete(public_path('upload/img/'.$array[5]));
        }

        $file       = $image;
        $file_name  = url('upload/img/'.$file->hashName());
        $destinasi  = 'upload/img/';
        $file->move($destinasi, $file_name);

        return $file_name;
    }

    public function delete($image)
    {
        $array = explode('/',$image);
        if(isset($array[5])){
            File::delete(public_path('upload/img/'.$array[5]));
        }
    }

    public function saveFlag($image)
    {
        $file       = $image;
        $file_name  = 'upload/img/'.$file->hashName();
        $destinasi  = public_path().'upload/img/';
        $file->move($destinasi, $file_name);

        return $file_name;
    }

    public function updateFlag($old_image, $image)
    {

        File::delete(public_path('upload/img/'.$old_image));

        $file       = $image;
        $file_name  = 'upload/img/'.$file->hashName();
        $destinasi  = public_path().'upload/img/';
        $file->move($destinasi, $file_name);

        return $file_name;
    }
}