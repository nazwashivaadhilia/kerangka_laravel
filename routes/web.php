<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ExampleController;
use App\Http\Controllers\Backend\VisiController;
use App\Http\Controllers\Backend\AboutController;
use App\Http\Controllers\Backend\BannerController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\PreferenceController;
use App\Http\Controllers\Backend\AbouttController;
use App\Http\Controllers\Backend\Quote1Controller;
use App\Http\Controllers\Backend\HeaderController;
use App\Http\Controllers\Backend\Quote2Controller;
use App\Http\Controllers\Backend\ContactController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [ExampleController::class, 'index']);

Route::group(['namespace' => 'App\Http\Controllers\Backend', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Auth::routes();
    Route::middleware('auth:web')->group(function () {

        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

        Route::group(['prefix' => 'banner', 'as' => 'banner.'], function () {
            Route::get('/', [BannerController::class, 'index'])->name('index');
            Route::get('/create', [BannerController::class, 'create'])->name('create');
            Route::post('/store', [BannerController::class, 'store'])->name('store');
            Route::get('edit/{banner}', [BannerController::class, 'edit'])->name('edit');
            Route::post('/update/{banner}', [BannerController::class, 'update'])->name('update');
            Route::get('delete/{banner}', [BannerController::class, 'delete'])->name('delete');
        });

        // Route::group(['prefix' => 'about', 'as' => 'about.'], function () {
        //     Route::get('/', [AboutController::class, 'index'])->name('index');
        //     Route::post('/store', [AboutController::class, 'store'])->name('store');
        //     Route::post('/update/{content}', [AboutController::class, 'update'])->name('update');
        // });

        Route::group(['prefix' => 'menu', 'as' => 'menu.'], function () {
            Route::get('/', [VisiController::class, 'index'])->name('index');
            Route::get('/create', [VisiController::class, 'create'])->name('create');
            Route::post('/store', [VisiController::class, 'store'])->name('store');
            Route::get('edit/{menu}', [VisiController::class, 'edit'])->name('edit');
            Route::post('/update/{menu}', [VisiController::class, 'update'])->name('update');
            Route::get('delete/{menu}', [VisiController::class, 'delete'])->name('delete');
        });

        Route::group(['prefix' => 'aboutt', 'as' => 'aboutt.'], function () {
            Route::get('/', [AbouttController::class, 'index'])->name('index');
            Route::post('/update/{aboutt}', [AbouttController::class, 'update'])->name('update');
        });

        Route::group(['prefix' => 'quote1', 'as' => 'quote1.'], function () {
            Route::get('/', [Quote1Controller::class, 'index'])->name('index');
            Route::post('/update/{quote1}', [Quote1Controller::class, 'update'])->name('update');
        });

        Route::group(['prefix' => 'header', 'as' => 'header.'], function () {
            Route::get('/', [HeaderController::class, 'index'])->name('index');
            Route::post('/update/{header}', [HeaderController::class, 'update'])->name('update');
        });

        Route::group(['prefix' => 'quote2', 'as' => 'quote2.'], function () {
            Route::get('/', [Quote2Controller::class, 'index'])->name('index');
            Route::post('/update/{quote2}', [Quote2Controller::class, 'update'])->name('update');
        });

        Route::group(['prefix' => 'contact', 'as' => 'contact.'], function () {
            Route::get('/', [ContactController::class, 'index'])->name('index');
            Route::post('/update/{contact}', [ContactController::class, 'update'])->name('update');
        });
    });
});
