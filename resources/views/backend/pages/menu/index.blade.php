{{-- @extends('backend.layouts.app')
@section('content')

<link rel="stylesheet" type="text/css" href="/backend/app-assets/css/core/menu/menu-types/vertical-menu.css">
<link rel="stylesheet" type="text/css" href="/backend/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
<link rel="stylesheet" type="text/css" href="/backend/app-assets/css/plugins/forms/pickers/form-pickadate.css">
<link rel="stylesheet" href="/backend/app-assets/css/uploader.css">
<link rel="stylesheet" href="/backend/app-assets/css/uploader-2.css">

<section class="home"><br>
    <!-- <div class="text">Dashboard</div> -->
    <div class="container">
        <h1>TABEL DATA MENU</h1>
        <!-- Di sini Anda dapat menambahkan tabel data menu jika diperlukan -->
    </div><br>
    <div class="outer-container">
        <a href={{ url('menu/create') }}><button class="coklat-btn1"><i class='bx bxs-message-square-add'></i></i> Add data
                menu</button></a>
        <div class="inner-container">
            <table>
                <thead>
                    <tr>
                        <th>ID Menu</th>
                        <th>Nomor Menu</th>
                        <th>Nama Menu</th>
                        <th>Deskripsi</th>
                        <th>Harga</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($menu as $m)
                    <tr>
                        <td>{{ $m->ID_menu }}</td>
                        <td>{{ $m->No_menu }}</td>
                        <td>{{ $m->Nama_menu }}</td>
                        <td>{{ $m->Deskripsi }}</td>
                        <td>{{ $m->Harga }}</td>
                        <td><a href="/menus/update/{{ $m->ID_menu }}"><button class="coklat-btn"><i class='bx bxs-edit bx-tada'></i></i> Update data</button></a></td>
                        <td><a href="/menu/delete/{{ $m->ID_menu }}"><button class="merah-btn"><i class='bx bxs-trash icon'></i> Delete</button></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>

@endsection --}}



@extends('backend.layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="/backend/backend/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/backend/backend/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
    <link rel="stylesheet" type="text/css" href="/backend/backend/app-assets/css/plugins/forms/pickers/form-pickadate.css">

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Menu</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Basic table -->
        <section id="ajax-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header border-bottom">
                            <h4 class="card-title">Menu List</h4>
                            <a href="{{ route('admin.menu.create') }}" class="btn btn-primary">Add Data</a>
                        </div>
                        <div class="card-body">
                            <table class="table" id="data-notif">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Menu</th>
                                        <th>Deskripsi</th>
                                        <th>Harga</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ Basic table -->
    </div>
@endsection

@section('script')
    <!-- BEGIN: Page Vendor JS-->
    <script src="/backend/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js" defer></script>
    <script src="/backend/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js" defer></script>
    <script src="/backend/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" defer></script>
    <script src="/backend/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js" defer></script>

    <script src="/backend/app-assets/js/scripts/tables/table-datatables-advanced.js" defer></script>
    <script src="/backend/app-assets/js/scripts/components/components-dropdowns.js"></script>
    <!-- END: Page Vendor JS-->
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            var table = $('#data-notif').DataTable({
                processing: true,
                serverSide: true,
                ajax: "/admin/menu",
                columns: [{
                        data: null,
                        "sortable": false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                    },
                    {
                        data: 'nomor_menu',
                        name: 'nomor_menu',
                        title: 'nomor_menu'
                    }, // Tambahkan kolom title
                    {
                        data: 'nama_menu',
                        name: 'nama_menu',
                        title: 'nama_menu'
                    },
                    {
                        data: 'deskripsi',
                        name: 'deskripsi',
                        title: 'deskripsi'
                    },
                    {
                        data: 'harga',
                        name: 'harga',
                        title: 'harga'
                    },
                    {
                        data: 'id',
                        render: (id) => /* html */ `
                <div class="btn-group">
                    <button class="btn btn-flat-dark dropdown-toggle" type="button" id="dropdownMenuButton100" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="/app-assets/images/align-justify.svg">
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/admin/menu/edit/${id}">Edit</a>
                        <a class="dropdown-item" href="javascript:void(0);" onclick="return hapus(${id})">Hapus</a>
                    </div>
                </div>
                `
                    },
                ],
                columnDefs: [{
                    className: 'control',
                    orderable: false,
                    targets: 0
                }],
                dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                orderCellsTop: true,
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal({
                            header: function(row) {
                                var data = row.data();
                                return 'Details of ' + data['full_name'];
                            }
                        }),
                        type: 'column',
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                            tableClass: 'table'
                        })
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                },
                drawCallback: () => {
                    $('.delete').click(function() {
                        const id = $(this).data(id)
                    })
                }
            });
        });

        function hapus(id) {
            var table = $('#data-notif').DataTable();
            clearToastObj = toastr['error'](
                'Are You Delete?<br /><br /><button type="button" class="btn btn-danger btn-sm delete">Yes</button>',
                'Deleted', {
                    closeButton: true,
                    timeOut: 0,
                    extendedTimeOut: 0,
                    tapToDismiss: false,
                }
            );

            if (clearToastObj.find('.delete').length) {
                clearToastObj.delegate('.delete', 'click', function() {
                    toastr.clear(clearToastObj, {
                        force: true
                    });
                    clearToastObj = undefined;
                    $.ajax({
                        method: "GET",
                        url: "/admin/menu/delete/" + id,
                        success: function(data) {
                            toastr['success']('Successfully Delete Data.', 'Successfully', {
                                closeButton: true,
                                tapToDismiss: false,
                                progressBar: true,
                            });
                            table.ajax.reload();
                        },
                        error: function(data) {
                            toastr['success'](
                                '👋 Chocolate oat cake jelly oat cake candy jelly beans pastry.',
                                'Progress Bar', {
                                    closeButton: true,
                                    tapToDismiss: false,
                                    progressBar: true,
                                    rtl: isRtl
                                });
                        }
                    });
                });
            }
        }
    </script>
    <script>
        $(function() {
            $('.hapus').on('click', function() {
                var id = $(this).attr('data-id');
                console.log(id);
            });
        });
    </script>
    <script>
        CKEDITOR.replace('editor1');
    </script>
    @if (Session::get('created'))
        <script type="text/javascript">
            $(document).ready(function() {
                toastr['success']('Successfully Create Data.', 'Successfully', {
                    closeButton: true,
                    tapToDismiss: false,
                    progressBar: true,
                });
            });
        </script>
    @endif

    @if (Session::get('updated'))
        <script type="text/javascript">
            $(document).ready(function() {
                toastr['success']('Successfully Update Data.', 'Successfully', {
                    closeButton: true,
                    tapToDismiss: false,
                    progressBar: true,
                });
            });
        </script>
    @endif
@endsection
