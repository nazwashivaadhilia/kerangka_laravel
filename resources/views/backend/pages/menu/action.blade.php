@extends('backend.layouts.app')
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <div class="breadcrumb-wrapper">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Menu</a>
                        </li>
                        <li class="breadcrumb-item active">
                            @if ($data == null)
                            Create Menu
                            @else
                            Update Menu
                            @endif
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">

    <!-- Validation -->
    <section class="bs-validation">
        @if ($data !== null)
        <form action="{{ route('admin.menu.update', $data->id) }}" method="POST" enctype="multipart/form-data">    
        @else
        <form action="{{ route('admin.menu.store') }}" id="jquery-val-form" method="POST" enctype="multipart/form-data">
        @endif
        @csrf
            <div class="row">
                <!-- Bootstrap Validation -->
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">
                                @if ($data == null)
                                Create Menu
                                @else
                                Update Menu
                                @endif
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">Nomor menu</label>
                                <input type="text" id="basic-addon-name" name="nomor_menu" class="form-control @error('nomor_menu') border-danger @enderror" aria-label="nomor_menu" value="{{ $data ? $data->nomor_menu : '' }}" aria-describedby="basic-addon-title" />
                                @error('nomor_menu')
                                    <span class="text-danger">*{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">Nomor menu</label>
                                <input type="text" id="basic-addon-name" name="nama_menu" class="form-control @error('nama_menu') border-danger @enderror" aria-label="nama_menu" value="{{ $data ? $data->nama_menu : '' }}" aria-describedby="basic-addon-title" />
                                @error('nama_menu')
                                    <span class="text-danger">*{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">Deskripsi</label>
                                <textarea name="deskripsi" id="descriptions"  cols="30" rows="10" class="form-control @error('deskripsi') border-danger @enderror">{{ $data ? $data->deskripsi : '' }}</textarea>
                                @error('deskripsi')
                                    <span class="text-danger">*{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">Harga</label>
                                <input type="text" id="basic-addon-name" name="harga" class="form-control @error('harga') border-danger @enderror" aria-label="harga" value="{{ $data ? $data->harga : '' }}" aria-describedby="basic-addon-title" />
                                @error('harga')
                                    <span class="text-danger">*{{ $message }}</span>
                                @enderror
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-md-6">
                    <button type="submit"
                    data-initial-text="Update <i class='icon-paperplane ml-2'></i>"
                    data-loading-text="<i class='fas fa-spinner fa-spin'></i> Loading..."
                    class="btn btn-primary"> Save Changes </button>
                </div>
            </div>
        </form>
    </section>
</div>

@endsection

@section('script')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
            $('.image-upload-wrap').hide();

            $('.file-upload-image').attr('src', e.target.result);
            $('.file-upload-content').show();

            $('.image-title').html(input.files[0].name);
            };

            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }

    $(function (){
        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });

        $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
        });
    });

</script>
@if(Session::get('create'))
    <script type="text/javascript">
        $(document).ready(function(){

            // Success Type
        toastr['success']('Successfully Create Data.', 'Successfully', {
            closeButton: true,
            tapToDismiss: false,
            progressBar: true,
        });

    });
</script>
@endif

@if(Session::get('update'))
<script type="text/javascript">
    $(document).ready(function(){

        // Success Type
        toastr['success']('Successfully Update Data.', 'Successfully', {
            closeButton: true,
            tapToDismiss: false,
            progressBar: true,
        });

    });
</script>
@endif
@endsection