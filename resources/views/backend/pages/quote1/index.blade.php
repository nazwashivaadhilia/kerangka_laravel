@extends('backend.layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="/backend/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/backend/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
    <link rel="stylesheet" type="text/css" href="/backend/app-assets/css/plugins/forms/pickers/form-pickadate.css">
    <link rel="stylesheet" href="/backend/app-assets/css/uploader.css">
    <link rel="stylesheet" href="/backend/app-assets/css/uploader-2.css">

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/admin">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">
                                Quote 1
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <!-- Validation -->
        <section class="bs-validation">
            @if ($data !== null)
                <form action="{{ route('admin.quote1.update', $data->id) }}" method="POST" enctype="multipart/form-data">
            @endif
            @csrf
            <div class="row">
                <!-- Bootstrap Validation -->
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Quote 1</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label" for="basic-addon-name">Title</label>
                                <input type="text" id="basic-addon-name" name="title"
                                    class="form-control @error('title') border-danger @enderror" aria-label="title"
                                    value="{{ $data && isset($data->Atribute['title']) ? $data->Atribute['title'] : '' }}"
                                    aria-describedby="basic-addon-title" />
                                @error('title')
                                    <span class="text-danger">*{{ $message }}</span>
                                @enderror

                                <div class="file-upload  @error('image') border-danger @enderror">
                                    <button class="file-upload-btn" type="button"
                                        onclick="$('.file-upload-input').trigger( 'click' )">Add image</button>
                                    <div class="image-upload-wrap">
                                        <input class="file-upload-input" type='file' onchange="readURL(this);"
                                            accept="image/*" name="image" />
                                        <div class="drag-text">
                                            @if ($data !== null)
                                                <img class="file-upload-image"
                                                    src="{{ $data && isset($data->Atribute['image']) ? $data->Atribute['image'] : '' }}"
                                                    alt="your image" />
                                            @else
                                                <h3>Drag and drop a file or select add image</h3>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="file-upload-content">
                                        <img class="file-upload-image" src="" alt="your image" />
                                        <div class="image-title-wrap">
                                            <button type="button" onclick="removeUpload()" class="remove-image">Remove
                                                <span class="image-title">Uploaded image</span></button>
                                        </div>
                                    </div>
                                    <small class="text-danger">** Max 1mb</small> <br>
                                    @error('image')
                                        <span class="text-danger">*{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col-md-6">
                        <button data-loading-text="Loading ..." type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
                </form>
        </section>
    </div>
@endsection

@section('script')
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
    @if (session('created'))
        <script>
            $(function() {
                toastr['success']('👋 Yes Data Created !!.', 'Created Success', {
                    closeButton: true,
                    tapToDismiss: false,
                    progressBar: true,
                });
            });
        </script>
    @elseif(session('updated'))
        <script>
            $(function() {
                toastr['success']('👋 Yes Data Updated !!.', 'Updated Success', {
                    closeButton: true,
                    tapToDismiss: false,
                    progressBar: true,
                });
            });
        </script>
    @endif
    <script>
        $(function() {
            var textarea = document.getElementById('descriptions');
            CKEDITOR.replace(textarea);

            var vision = document.getElementById('vision');
            CKEDITOR.replace(vision);

            var mission = document.getElementById('mission');
            CKEDITOR.replace(mission);

            var our_values = document.getElementById('our_value');
            CKEDITOR.replace(our_values);

            var description = document.getElementById('description_1');
            CKEDITOR.replace(description);

            var description_2 = document.getElementById('description_2');
            CKEDITOR.replace(description_2);
        });
    </script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.image-upload-wrap').hide();

                    $('.file-upload-image').attr('src', e.target.result);
                    $('.file-upload-content').show();

                    $('.image-title').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }

        $(function() {
            $('.image-upload-wrap').bind('dragover', function() {
                $('.image-upload-wrap').addClass('image-dropping');
            });

            $('.image-upload-wrap').bind('dragleave', function() {
                $('.image-upload-wrap').removeClass('image-dropping');
            });
        });

        function readURL2(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.image-upload-wrap-2').hide();

                    $('.file-upload-image-2').attr('src', e.target.result);
                    $('.file-upload-content-2').show();

                    $('.image-title-2').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload2();
            }
        }

        function removeUpload2() {
            $('.file-upload-input-2').replaceWith($('.file-upload-input-2').clone());
            $('.file-upload-content-2').hide();
            $('.image-upload-wrap-2').show();
        }

        $(function() {
            $('.image-upload-wrap-2').bind('dragover', function() {
                $('.image-upload-wrap-2').addClass('image-dropping-2');
            });

            $('.image-upload-wrap-2').bind('dragleave', function() {
                $('.image-upload-wrap-2').removeClass('image-dropping-2');
            });
        });
    </script>
@endsection
