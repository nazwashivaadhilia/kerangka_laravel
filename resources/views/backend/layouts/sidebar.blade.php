<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ url('/') }}">
                    <span class="brand-logo">
                        <img src="https://swan.sobatteknologi.com/images/logo-img.svg"
                            style="max-width: 147px !important; margin-left: 22px;margin-bottom: 20px !important;">
                    </span>
                </a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages"> Swan</span><i
                    data-feather="more-horizontal"></i>
            </li>

            <li class="nav-item {{ request()->routeIs('admin.dashboard') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="">
                    <i data-feather='cast'></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Dashboard</span>
                </a>
            </li>

            {{-- <li class="nav-item {{ request()->routeIs('admin.banner*') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ route('admin.banner.index') }}">
                    <i data-feather='image'></i>
                    <span class="menu-title text-truncate" data-i18n="Banner">Banner</span>
                </a>
            </li> --}}

            {{-- <li class="nav-item {{ request()->routeIs('admin.about*') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ route('admin.about.index') }}">
                    <i data-feather='archive'></i>
                    <span class="menu-title text-truncate" data-i18n="About Us">About Us</span>
                </a>
            </li> --}}


            <li class="nav-item {{ request()->routeIs('admin.menu*') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ route('admin.menu.index') }}">
                    <i data-feather='archive'></i>
                    <span class="menu-title text-truncate" data-i18n="Visi Misi">Menu</span>
                </a>
            </li>

            <li class="nav-item {{ request()->routeIs('admin.aboutt*') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ route('admin.aboutt.index') }}">
                    <i data-feather='archive'></i>
                    <span class="menu-title text-truncate" data-i18n="Aboutt">Aboutt</span>
                </a>
            </li>

            <li class="nav-item {{ request()->routeIs('admin.quote1*') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ route('admin.quote1.index') }}">
                    <i data-feather='archive'></i>
                    <span class="menu-title text-truncate" data-i18n="quote1">Quote 1</span>
                </a>
            </li>

            <li class="nav-item {{ request()->routeIs('admin.header*') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ route('admin.header.index') }}">
                    <i data-feather='archive'></i>
                    <span class="menu-title text-truncate" data-i18n="header">Header</span>
                </a>
            </li>

            <li class="nav-item {{ request()->routeIs('admin.quote2*') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ route('admin.quote2.index') }}">
                    <i data-feather='archive'></i>
                    <span class="menu-title text-truncate" data-i18n="quote2">Quote 2</span>
                </a>
            </li>

            <li class="nav-item {{ request()->routeIs('admin.contact*') ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ route('admin.contact.index') }}">
                    <i data-feather='archive'></i>
                    <span class="menu-title text-truncate" data-i18n="contact">Contact</span>
                </a>
            </li>
            <!-- <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='home'></i><span class="menu-title text-truncate" data-i18n="Invoice">Menu</span></a>
                <ul class="menu-content">
                    <li class="">
                        <a class="d-flex align-items-center" href="">
                            <i data-feather='image'></i>
                            <span class="menu-title text-truncate" data-i18n="Dashboard">Sub Menu</span>
                        </a>
                    </li>
                </ul>
            </li> -->
        </ul>
    </div>
</div>
<!-- END: Main Menu -->
