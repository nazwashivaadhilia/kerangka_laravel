<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Preference;

class PreferenceSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pref = [
            [
                'type' => 'aboutt',
                'Atribute' => [
                    'title' => 'My daily routine is to check if the flower is happy enough',
                    'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat a fuga corrupti inventore officia porro nulla omnis molestiae doloremque nihil officiis ex, nulla laborum voluptatibus ab maxime reprehenderit accusantium. Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                    'sub' => 'Certified for florist from Bandung Florist Academy',
                    'image' => 'img/banner-3.jpeg'
                ]
            ],
            [
                'type' => 'contact',
                'Atribute' => [
                    'title' => 'Call +66 1234 56789 to get free consultation',
                    'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                    'email' => 'crativaflorist@mail.com',
                    'phone' => '+62 22 1234 56789',
                    'loc' => 'Jakarta, Indonesia',
                    'image' => 'img/banner-4.jpeg'
                ]
            ],
            [
                'type' => 'quote2',
                'Atribute' => [
                    'title' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatibus, impedit eaque omnis error asperiores sequi dolorem soluta totam, aspernatur quam. '
                ]
            ],
            [
                'type' => 'quote1',
                'Atribute' => [
                    'title' => 'I have created tons of beautiful flower/bucket for your events ',
                    'image' => 'img/banner-2.jpeg'
                ]
            ],
            [
                'type' => 'header',
                'Atribute' => [
                    'title' => 'The florist you looking for ',
                    'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eligendi blanditiis sapiente non excepturi. ',
                    'image' => 'img/banner-1.jpeg'
                ]
            ]
        ];

        foreach ($pref as $data) {
            Preference::create([
                'type' => $data['type'],
                'Atribute' => $data['Atribute'], // Simpan atribut sebagai JSON jika diperlukan
            ]);
        }
    }
}
